<?php

namespace App\Tests\Model;

use App\Fixtures\Employees;
use App\Model\Calculator;
use App\Model\CalculatorInterface;
use App\Model\Data\EmployeeCollection;
use App\Model\Data\EmployeeData;
use App\Model\Data\EmployeeFilter;
use PHPUnit\Framework\TestCase;

/**
 * Class CalculatorTest
 * @package App\Tests\Model
 * @covers \App\Model\Calculator
 */
class CalculatorTest extends TestCase
{

    /**
     * @var EmployeeCollection
     */
    protected $collection;

    /**
     * @var CalculatorInterface
     */
    protected $calculator;

    /**
     * @SuppressWarnings(StaticAccess)
     */
    public function setUp()
    {
        $this->calculator = new Calculator();

        $fixture = new Employees();

        $data = [];

        foreach ($fixture() as $entry) {
            $data[] = EmployeeData::fromArray($entry);
        }

        $this->collection = new EmployeeCollection($data);
    }

    public function testCalculateAdditionalDays()
    {
        $this->assertTrue($this->calculate('Hans Müller', 2050) === 35);
        $this->assertTrue($this->calculate('Hans Müller', 2018) === 29);
    }

    public function testCalculateFreshmanDays()
    {
        $this->assertTrue($this->calculate('Sepp Meier', 2017) === 0);
        $this->assertTrue($this->calculate('Marina Helter', 2018) === 21);
    }

    public function testCalculateSpecialContract()
    {
        $this->assertTrue($this->calculate('Peter Klever', 2017) === 27);
        $this->assertTrue($this->calculate('Peter Klever', 2050) === 27);
    }

    /**
     * @param string $name
     * @param int $year
     * @return int|null
     */
    protected function calculate(string $name, int $year)
    {
        $collection = new EmployeeFilter($this->collection, [
            'name' => $name
        ]);

        foreach ($collection as $employee) {
            return $this->calculator->calculate($employee, $year);
        }

        return null;
    }
}
