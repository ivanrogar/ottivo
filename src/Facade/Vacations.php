<?php

declare(strict_types=1);

namespace App\Facade;

use App\Entity\Employee;
use App\Model\CalculatorInterface;
use App\Model\Data\EmployeeCollection;
use App\Model\Data\EmployeeData;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class Vacations
 * @package App\Facade
 */
final class Vacations
{

    private $registry;
    private $calculator;

    /**
     * Vacations constructor.
     * @param ManagerRegistry $registry
     * @param CalculatorInterface $calculator
     */
    public function __construct(
        ManagerRegistry $registry,
        CalculatorInterface $calculator
    ) {
        $this->registry = $registry;
        $this->calculator = $calculator;
    }

    /**
     * @param int $year
     * @return EmployeeCollection
     * @SuppressWarnings(StaticAccess)
     */
    public function byYear(int $year)
    {
        $employees = $this->getEmployees();
        $results = [];

        foreach ($employees as $employee) {
            // - we should favor DTOs over actual entities at all times where possible
            // - although we could've done the calculation on the entity/domain logic, we didn't
            // for the sake of eventual future complexity of this "app"
            $employeeData = EmployeeData::fromEmployee($employee);

            $this->calculator->calculate($employeeData, $year);

            $results[] = $employeeData;
        }

        return new EmployeeCollection($results);
    }

    /**
     * @return Employee[]
     */
    private function getEmployees()
    {
        return $this
            ->registry
            ->getRepository(Employee::class)
            ->findAll();
    }
}
