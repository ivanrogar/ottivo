<?php

declare(strict_types=1);

namespace App\Fixtures;

/**
 * Class Employees
 * @package App\Fixtures
 */
class Employees
{

    /**
     * @return array
     */
    public function __invoke(): array
    {
        return [
            [
                'name' => 'Hans Müller',
                'dob' => '1950-12-30',
                'startDate' => '2001-01-01',
                'specialContract' => null
            ],
            [
                'name' => 'Angelika Fringe',
                'dob' => '1966-06-09',
                'startDate' => '2001-01-15',
                'specialContract' => null
            ],
            [
                'name' => 'Peter Klever',
                'dob' => '1991-07-12',
                'startDate' => '2016-05-15',
                'specialContract' => '27 vacation days',
            ],
            [
                'name' => 'Marina Helter',
                'dob' => '1970-01-26',
                'startDate' => '2018-01-15',
                'specialContract' => null
            ],
            [
                'name' => 'Sepp Meier',
                'dob' => '1980-05-23',
                'startDate' => '2017-12-01',
                'specialContract' => null,
            ],
        ];
    }
}
