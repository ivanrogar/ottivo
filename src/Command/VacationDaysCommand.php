<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Facade\Vacations;
use \DateTime;
use \DateTimeZone;

/**
 * Class VacationDaysCommand
 * @package App\Command
 */
class VacationDaysCommand extends Command
{

    protected static $defaultName = 'ottivo:employee:vacation-days';
    protected $vacations;

    /**
     * VacationDaysCommand constructor.
     * @param Vacations $vacations
     * @param null $name
     */
    public function __construct(
        Vacations $vacations,
        $name = null
    ) {
        $this->vacations = $vacations;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Get vacation days for a given year')
            ->addOption('year', 'otva', InputOption::VALUE_REQUIRED, 'Input Year');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     * @SuppressWarnings(ShortVariable)
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $io = new SymfonyStyle($input, $output);

        $year = $input->getOption('year');

        if (!$year) {
            $year = (new DateTime('now', new DateTimeZone('UTC')))->format('Y');
            $io->title('Year was not set, using year value: ' . $year);
        }

        $data = [
            'year' => $year,
            'employees' => [],
        ];

        $results = $this->vacations->byYear((int) $year);

        foreach ($results as $result) {
            $io->writeln($result->name . ': ' . $result->vacationDays . ' days');
            $data['employees'][] = [
                $result->name => $result->vacationDays
            ];
        }

        $outputPath = dirname(__FILE__) . '/../../var/vacations.json';

        file_put_contents($outputPath, json_encode($data, JSON_PRETTY_PRINT));

        $io->note('Data written to ' . $outputPath);

        return 0;
    }
}
