<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Employee;
use App\Fixtures\Employees;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\Common\Persistence\ManagerRegistry;
use \DateTime;

/**
 * Class InstallCommand
 * @package App\Command
 */
class InstallCommand extends Command
{

    protected static $defaultName = 'ottivo:install';
    protected $registry;

    /**
     * InstallCommand constructor.
     * @param ManagerRegistry $registry
     * @param null $name
     */
    public function __construct(ManagerRegistry $registry, $name = null)
    {
        $this->registry = $registry;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Ottivo installer');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     * @SuppressWarnings(ShortVariable)
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $io = new SymfonyStyle($input, $output);

        $ema = $this->registry->getManager();
        $repository = $this->registry->getRepository(Employee::class);

        if ($repository->getEmployeeCount()) {
            $io->error('It seems like the fixture data has already been installed.');
            return -1;
        }

        $fixture = new Employees();

        foreach ($fixture() as $entry) {
            $io->note(' -> adding: ' . $entry['name']);

            $employee = new Employee();

            $employee
                ->setName($entry['name'])
                ->setDob(new DateTime($entry['dob']))
                ->setStartDate(new DateTime($entry['startDate']))
                ->setSpecialContract($entry['specialContract']);

            $ema->persist($employee);
            $ema->flush();
            $ema->clear();
        }

        return 0;
    }
}
