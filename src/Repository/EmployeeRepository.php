<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class EmployeeRepository
 * @package App\Repository
 */
class EmployeeRepository extends EntityRepository
{

    /**
     * @return int|null
     */
    public function getEmployeeCount(): ?int
    {
        try {
            return (int) $this
                ->createQueryBuilder('q')
                ->select('COUNT(q.id) as employeeCount')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
