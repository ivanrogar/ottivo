<?php

declare(strict_types=1);

namespace App\Model;

use App\Model\Data\EmployeeData;
use App\Model\Rules\Vacations\General;
use \DateTime;
use \DateTimeZone;

/**
 * Class Calculator
 * @package App\Model
 */
class Calculator implements CalculatorInterface
{

    /**
     * @var EmployeeData
     */
    protected $data;

    /**
     * @var DateTime
     */
    protected $calculationDate;

    /**
     * @inheritdoc
     */
    public function calculate(EmployeeData $data, int $year): int
    {
        $this->data = $data;
        $this->calculationDate = new DateTime('now', new DateTimeZone('UTC'));
        $this->calculationDate->setDate($year, 12, 31);

        $this->data->vacationDays = $this->process();

        return $this->data->vacationDays;
    }

    /**
     * @return int
     */
    protected function process(): int
    {
        $date = $this->calculationDate;

        $employee = $this->data;

        $currentYear = (int) $date->format('Y');
        $employeeStartYear = (int) $employee->startDate->format('Y');

        // employee doesn't "exist" yet in Ottivo
        if ($currentYear < $employeeStartYear) {
            return 0;
        }

        // has special contract which overrides the calculation ?
        if ($employee->specialContract) {
            return (int) $employee->specialContract;
        }

        $days = General::MINIMUM_DAYS;

        // is it a "fresh" employee ?
        if ($currentYear === $employeeStartYear) {
            $ratio = $days / General::FIRST_YEAR_OF_SERVICE_RATIO;

            $startingFrom = clone $employee->startDate;

            $startingFrom->modify('first day of next month');

            // considering the coding task description
            // it's not quite clear if the employees accumulate one twelfth of vacation days month by month
            // throughout their starting year or they have only one twelfth of minimum days throughout their first year
            $month = (int) $startingFrom->format('m');

            // we leaped into next year
            if ($month === 1) {
                return 0;
            }

            $months = ($month === 12)
                ? 1
                : 12 - $month;

            return (int) ($months * $ratio);
        }

        $days += $this->getAdditionalDays();

        return $days;
    }

    /**
     * @return int
     */
    protected function getAdditionalDays(): int
    {
        $rules = General::ADDITIONAL_DAYS_BY_DOB;

        krsort($rules);

        $employeeAge = $this->data->dob->diff($this->calculationDate)->y;
        $serviceAge = $this->data->startDate->diff($this->calculationDate)->y;

        $additionalDays = 0;

        foreach ($rules as $age => $rule) {
            if ($employeeAge >= $age) {
                $total = $serviceAge / $rule['interval'];

                if ($total > 0) {
                    $additionalDays = (int) ($total * $rule['days']);
                    break;
                }
            }
        }

        return $additionalDays;
    }
}
