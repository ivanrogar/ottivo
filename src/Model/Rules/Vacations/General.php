<?php

declare(strict_types=1);

namespace App\Model\Rules\Vacations;

/**
 * Class General
 * @package App\Model\Rules\Vacations
 */
class General
{

    const MINIMUM_DAYS = 26;

    const FIRST_YEAR_OF_SERVICE_RATIO = 12;

    const ADDITIONAL_DAYS_BY_DOB = [
        30 => [
            'interval' => 5,
            'days' => 1,
        ]
    ];
}
