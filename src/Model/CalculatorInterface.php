<?php

declare(strict_types=1);

namespace App\Model;

use App\Model\Data\EmployeeData;

/**
 * Interface CalculatorInterface
 * @package App\Model
 */
interface CalculatorInterface
{

    /**
     * @param EmployeeData $employeeData
     * @param int $year
     * @return int
     */
    public function calculate(EmployeeData $employeeData, int $year): int;
}
