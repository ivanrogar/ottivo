<?php

declare(strict_types=1);

namespace App\Model\Data;

use \FilterIterator;

/**
 * Class EmployeeFilter
 * @package App\Model\Data
 */
class EmployeeFilter extends FilterIterator
{

    private $filters = [];

    /**
     * EmployeeFilter constructor.
     * @param EmployeeCollection $iterator
     * @param array $filters
     */
    public function __construct(EmployeeCollection $iterator, array $filters)
    {
        parent::__construct($iterator);
        $this->filters = $filters;
    }

    /**
     * @inheritDoc
     */
    public function accept()
    {
        /**
         * @var EmployeeData $entry
         */
        $entry = $this->getInnerIterator()->current();

        foreach ($this->filters as $field => $value) {
            if (property_exists($entry, $field)) {
                if ($entry->$field !== $value) {
                    return false;
                }
            }
        }

        return true;
    }
}
