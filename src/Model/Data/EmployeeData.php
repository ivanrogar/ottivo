<?php

declare(strict_types=1);

namespace App\Model\Data;

use App\Entity\Employee;
use Spatie\DataTransferObject\DataTransferObject;
use \DateTime;

/**
 * Class EmployeeData
 * @package App\Model\Data
 */
class EmployeeData extends DataTransferObject
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var DateTime
     */
    public $dob;

    /**
     * @var DateTime
     */
    public $startDate;

    /**
     * @var null|string
     */
    public $specialContract;

    /**
     * @var null|int
     */
    public $vacationDays;

    /**
     * @param Employee $employee
     * @return EmployeeData
     */
    public static function fromEmployee(Employee $employee): self
    {
        return new self([
            'name' => $employee->getName(),
            'dob' => $employee->getDob(),
            'startDate' => $employee->getStartDate(),
            'specialContract' => $employee->getSpecialContract(),
            'vacationDays' => 0,
        ]);
    }

    /**
     * @param array $data
     * @return EmployeeData
     */
    public static function fromArray(array $data): self
    {
        return new self([
            'name' => $data['name'],
            'dob' => new DateTime($data['dob']),
            'startDate' => new DateTime($data['startDate']),
            'specialContract' => $data['specialContract'],
            'vacationDays' => 0,
        ]);
    }
}
