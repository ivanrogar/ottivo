<?php

declare(strict_types=1);

namespace App\Model\Data;

use \Spatie\DataTransferObject\DataTransferObjectCollection;

/**
 * Class EmployeeCollection
 * @package App\Model\Data
 */
class EmployeeCollection extends DataTransferObjectCollection
{

    /**
     * @return EmployeeData
     */
    public function current(): EmployeeData
    {
        return parent::current();
    }
}
