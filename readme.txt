
install:
composer install
[ setup database in env file ]
./bin/console doctrine:database:create
./bin/console doctrine:schema:create
./bin/console ottivo:install

usage:
./bin/console ottivo:employee:vacation-days --year 2018

If option is not set, the default is current year.

test:
./bin/phpunit
